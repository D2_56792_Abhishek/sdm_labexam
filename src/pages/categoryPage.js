import axios from 'axios'
import { useEffect, useState } from 'react'

const CategoryPage = () => {
  const url = 'http://category-service:4000/category'
  const [categories, setCategories] = useState([])

  useEffect(() => {
    getCategories()
  }, [])

  const getCategories = () => {
    axios.get(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        setCategories(result['data'])
      }
    })
  }

  return (
    <div>
      <h1>Category List</h1>
      <ul className="list-group">
        {categories.map((category) => {
          return <li className="list-group-item">{category['Title']}</li>
        })}
      </ul>
    </div>
  )
}

export default CategoryPage
