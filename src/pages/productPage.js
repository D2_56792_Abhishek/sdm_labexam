import axios from 'axios'
import { useEffect, useState } from 'react'

const ProductPage = () => {
  const url = 'http://product-service:4000/product'
  const [products, setProducts] = useState([])

  useEffect(() => {
    getProducts()
  }, [])

  const getProducts = () => {
    axios.get(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        setProducts(result['data'])
      }
    })
  }

  return (
    <div>
      <h1>Product List</h1>
      <table className="table table-stripped">
        <thead>
          <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Description</th>
            <th>Price</th>
          </tr>
        </thead>

        <tbody>
          {products.map((product) => {
            return (
              <tr>
                <td>{product['Id']}</td>
                <td>{product['Title']}</td>
                <td>{product['Description']}</td>
                <td>{product['Price']}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

export default ProductPage
