FROM httpd

# copy the build to htdocs
COPY build/* /usr/local/apache2/htdocs/

EXPOSE 80
